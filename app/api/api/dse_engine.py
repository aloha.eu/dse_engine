# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import Flask, request, g

from . import Resource
from .. import schemas

# the following two imports are needed for create a new connection
from settings import settings
from db.mongo import MongoConnector

from rq import Queue
from worker import *

from models.models import *

from bson import ObjectId

from uuid import uuid4

import requests

import os
import subprocess

import logging
import time
from satellite_tools import *

import redis_lock
import traceback

import json
from collections import OrderedDict

CB_WAIT_TIME = 5
SHARED_DATA_FOLDER = "/opt/data/"
LOGGING_FOLDER = os.path.join(SHARED_DATA_FOLDER, "experiments/prj_%s/dse_engine_logs/") # to be completed with the project_id


DSE_TOOL_PATH = "/opt/www/modules/gridsearch/"

TASKS_QUEUE = []

MAX_PARALLEL = {'training'   : 1,
                'power_perf' : 10,
                'pareto'     : 10,
                'security'   : 10,
                'parsim'     : 10,
                'finalize'   : 10,
                'parsim'     : 10,
  }



def create_empty_AlgorithmConfiguration(path, project_id):
  
        # create a new design point (algorithm, ONNX)
        algorithm_configuration         = AlgorithmConfiguration()
        algorithm_configuration.project = str(project_id)




        algorithm_configuration.onnx = path

        ################## only for test ###############

        algorithm_configuration.training_accuracy   = 0
        algorithm_configuration.training_loss       = 0
        algorithm_configuration.validation_accuracy = 0
        algorithm_configuration.validation_loss     = 0


        # power performance evaluation
        algorithm_configuration.performance    = 0 # seconds
        algorithm_configuration.energy         = 0.0 # joule
        algorithm_configuration.processors     = 0 # minimun number of processor
        algorithm_configuration.memory         = 0.0 # bytes


        # security
        algorithm_configuration.sec_level      = "none" # low, medium, high
        algorithm_configuration.sec_value      = 0.0 #


        # images from tools
        algorithm_configuration.training_img_path        = "none"
        algorithm_configuration.power_perf_eval_img_path = "none"
        algorithm_configuration.rpi_img_path             = "none"
        algorithm_configuration.security_img_path        = "none"

        # images from tools
        algorithm_configuration.training_log_path        = "none"
        algorithm_configuration.power_perf_eval_log_path = "none"
        algorithm_configuration.rpi_log_path             = "none"
        algorithm_configuration.security_log_path        = "none"


        ###########################################


        algorithm_configuration.save()
        
        return algorithm_configuration
        
        
def post_security(project_id=None, algorithm_id=None, algorithm=None, job_id=None, **kwargs):
            
  ret = security_eval_getresults(algorithm=algorithm, job_id=job_id)
  if ret =='OK':
    logging.info('Security evaluation done. DSE point id: %s' % algorithm_id)
    send_back_status(project_id, "Running", "Security evaluation complete", "sec_en", "Finished")
  else:
    logging.info('Security evaluation failed. DSE point id: %s' % algorithm_id)
    send_back_status(project_id, "Errors", "security failed", "sec_en", "Failed")
    return 'failed'
    

def post_parsim(project_id=None, algorithm_id=None, algorithm=None, job_id=None, **kwargs):
  
  ret = parsim_getresults(algorithm=algorithm, job_id=job_id)
  if ret == 'OK':
    logging.info('Refinement for parsimonious inference done. DSE point id: %s' % algorithm_id)
    send_back_status(project_id, "Running", "Refinement for parsimonious inference complete", "rpi_en", "Finished")
  else:
    logging.info('Refinement for parsimonious inference failed. DSE point id: %s' % algorithm_id)
    send_back_status(project_id, "Errors", "RPI failed", "rpi_en", "Failed")
    return 'failed'
    
    
def post_power_perf(project_id=None, algorithm_id=None, algorithm=None, job_id=None, **kwargs):
  
  logging.info('Power Performance evaluation done. DSE point id: %s' % algorithm_id)
  send_back_status(project_id, "Running", "power_perf complete", "per_en", "Finished")



def post_training(project_id=None, algorithm_id=None, algorithm=None, job_id=None, **kwargs):
 
 
  algorithm.reload()
  status = training_getStatus(kwargs['callback_id'])
  

  #print (status)
  
  if status == 'ERROR':
    logging.info('Training failed. DSE point id: %s' % algorithm_id)
    
    res = {"message"  : "Discarded because of error in training",
           "training" : 'FAIL'
           }
           
    algorithm.training_log = json.dumps(res)
    algorithm.save()
    
    
    remove_from_queue (algorithm_id, status="failed")
    logging.info('Training done. DSE point id: %s' % algorithm_id)
    send_back_status(project_id, "Running", "training error", "tra_en", "Finished")
    return 'failed'
  else:
    logging.info('Training done. DSE point id: %s' % algorithm_id)
    send_back_status(project_id, "Running", "training complete", "tra_en", "Finished")
            



def pareto (project_title=None, algorithm=None, path = None, callback_id=None, **kwargs):
  algorithm.reload()
  # set up tbx
  from tensorboardX import SummaryWriter
  tbx_name = project_title.replace(' ','_') + "-" + os.path.basename(algorithm.onnx)[:-5] + "_perf"
  tbx_writer = SummaryWriter(log_dir=os.path.join(path, 'runs/%s' % tbx_name))

  # tensorboardX
  if tbx_writer is not None:

    tbx_writer.add_scalar('DSE/pareto/time_us',    algorithm.validation_accuracy, algorithm.performance*1000)
    tbx_writer.add_scalar('DSE/pareto/memory_kB',  algorithm.validation_accuracy, algorithm.memory/1024)
    tbx_writer.add_scalar('DSE/pareto/energy_mJ',  algorithm.validation_accuracy, algorithm.energy*1000)
  
  free_callback_id(callback_id)
  
  return None, 0

def sendGA (algorithm=None, key=None, callback_id=None, **kwargs):
  redisc=RedisConnector()
 
  algorithm.reload()
  
  acc=algorithm.validation_accuracy
  mem=algorithm.memory
  ene=algorithm.energy
  
  # redis set results
  
  logging.info('\tSending results to GA')
  
  res_key = key.replace ("path","res")
  
  logging.info('\t\t{} {}'.format (res_key+':acc', acc))
  logging.info('\t\t{} {}'.format (res_key+':mem', mem))
  logging.info('\t\t{} {}'.format (res_key+':ene', ene))
  
  redisc.conn.set(res_key+':acc', acc)
  redisc.conn.set(res_key+':mem', mem)
  redisc.conn.set(res_key+':ene', ene)


  free_callback_id(callback_id)
  
  return None, 0

def save_config (algorithm=None,training_en=False, power_perf_eval_en=False, status='OK', algorithm_id=None, project_id=None, callback_id=None, **kwargs):
  
  print ("Saving configuration files...")
  algorithm.reload()
  
  try:
    path = os.path.split(os.path.split(algorithm.onnx_trained)[0])[0]
    filenamejson = os.path.join(SHARED_DATA_FOLDER, path, "AlgorithmConfiguration.json")
  
  
    algorithm_str = algorithm.to_json().replace(', "', ',\n"')
  
    # retrive the config
    try:
        config = ConfigurationFile.objects.get(project=str(project_id))
        config_str = ',\n"ConfigurationFile": '+ config.to_json().replace(', "', ',\n"')
        
    except ConfigurationFile.DoesNotExist: # if the project doesn't exist return an error
        logging.error ("ERROR! ConfigurationFile doesn't exist")
        return
    except ConfigurationFile.MultipleObjectsReturned:
        logging.error ("ERROR! Multiple ConfigurationFile")
        return
  
    algorithm_str=algorithm_str[:-1] + config_str + '}'
  
    with open(filenamejson, 'w') as f: 
      f.write(algorithm_str) 
    print (algorithm_str)
    
    
    try:
        pipeline = Pipeline.objects.get(id=ObjectId(algorithm.pipeline))
        
        pipeline_str = pipeline.to_json().replace(', "', ',\n"')

    except Pipeline.DoesNotExist: # if the project doesn't exist return an error
        logging.error ("ERROR! Pipeline doesn't exist")
        return
    except Pipeline.MultipleObjectsReturned:
        logging.error ("ERROR! Multiple Pipelines")
        return
    
    path = os.path.split(os.path.split(algorithm.onnx_trained)[0])[0]
    filenamejson = os.path.join(SHARED_DATA_FOLDER, path, "preprocessing_pipeline.json")
    
    with open(filenamejson, 'w') as f: 
      f.write(pipeline_str)
    
  except:
    pass
  
  free_callback_id(callback_id)
  print ("done saving")
  return None, 0


def evaluation_complete (algorithm=None,training_en=False, power_perf_eval_en=False, status='OK', algorithm_id=None, project_id=None, callback_id=None, **kwargs):
  redisc=RedisConnector()
 
  algorithm.reload()
  
  try:
    path = os.path.split(os.path.split(algorithm.onnx_trained)[0])[0]
    filenamejson = os.path.join(SHARED_DATA_FOLDER, path, "AlgorithmConfiguration.json")
  
  
    algorithm_str = algorithm.to_json().replace(', "', ',\n"')
  
    # retrive the config
    try:
        config = ConfigurationFile.objects.get(project=str(project_id))
        config_str = ',\n"ConfigurationFile": '+ config.to_json().replace(', "', ',\n"')
        
    except ConfigurationFile.DoesNotExist: # if the project doesn't exist return an error
        logging.error ("ERROR! ConfigurationFile doesn't exist")
        return
    except ConfigurationFile.MultipleObjectsReturned:
        logging.error ("ERROR! Multiple ConfigurationFile")
        return
  
    algorithm_str=algorithm_str[:-1] + config_str + '}'
  
    with open(filenamejson, 'w') as f: 
      f.write(algorithm_str) 
    print (algorithm_str)
    
    
    try:
        pipeline = Pipeline.objects.get(id=ObjectId(algorithm.pipeline))
        
        pipeline_str = pipeline.to_json().replace(', "', ',\n"')

    except Pipeline.DoesNotExist: # if the project doesn't exist return an error
        logging.error ("ERROR! Pipeline doesn't exist")
        return
    except Pipeline.MultipleObjectsReturned:
        logging.error ("ERROR! Multiple Pipelines")
        return
    
    path = os.path.split(os.path.split(algorithm.onnx_trained)[0])[0]
    filenamejson = os.path.join(SHARED_DATA_FOLDER, path, "preprocessing_pipeline.json")
    
    with open(filenamejson, 'w') as f: 
      f.write(pipeline_str)
    
  except:
    pass
  
  

  
  
  try:
      if training_en:
          logging.info('Execution time')
          logging.info('Total time [sec]: %d' % sum(algorithm.execution_time))
          logging.info('Max time [sec]:   %d' % max(algorithm.execution_time))
          logging.info('Min time [sec]:   %d' % min(algorithm.execution_time))
          logging.info('Avg time [sec]:   %d' % (sum(algorithm.execution_time)/len(algorithm.execution_time)))

      if power_perf_eval_en:
          logging.info('Power perf')
          logging.info('performance:  %f' % algorithm.performance)
          logging.info('energy:       %f' % algorithm.energy)
          logging.info('processors:   %f' % algorithm.processors)
          logging.info('memory:       %f' % algorithm.memory)
  except:
      pass
  
  logging.info('################################################################################')
  logging.info('\tEVALUATION {}. Evaluation finished on DSE point id {}'.format("COMPLETED" if status=='OK' else "FAILED", algorithm_id))
  logging.info('################################################################################')
  
  free_callback_id(callback_id)
  
  return None, 0

def remove_from_queue(algorithm_id, status="completed"):
  for task in TASKS_QUEUE:
    if task['kwargs']['algorithm_id'] == str(algorithm_id):
      if not task['status'] == 'completed' and not task['status'] == 'failed': 
        free_callback_id(task['id'])
        task['status'] = status
  
  
  
def evaluate_constraints(constraints=None, algorithm=None, callback_id=None, **kwargs):
  
  algorithm.reload()
  
  #constraints.memory_priority
  #constraints.execution_time_priority

  exe = algorithm.performance < constraints.execution_time_value
  ene = algorithm.energy < constraints.power_value
  mem = algorithm.memory < constraints.memory_value

  exe_col='\33[41m' if not exe else '\033[0m'
  ene_col='\33[41m' if not ene else '\033[0m'
  mem_col='\33[41m' if not mem else '\033[0m'
  
  print ('\n##################### Constraints ######################')
  print ("%-20s\t%-20s\t%-20s\t"%("Metric", "Required", "Actual"))
  print (exe_col + "%-20s\t%-20s\t%-20s\t"%("Execution time  [ms]: " , str(constraints.execution_time_value), str(algorithm.performance)) + "\033[0m")
  print (ene_col + "%-20s\t%-20s\t%-20s\t"%("Energy  [J]: "          , str(constraints.power_value), str(algorithm.energy)) + "\033[0m")
  print (mem_col + "%-20s\t%-20s\t%-20s\t"%("Memory footprint [B]: " , str(constraints.memory_value), str(algorithm.memory)) + "\033[0m")
  print ('\n##################################################')

  if (not mem or not exe or not ene):
    print ("The design point {} does not satisfy the constraints".format(algorithm.id))
    
    res = {"message"        : "Discarded because of constraints",
           "execution_time" : 'OK' if exe else 'FAIL',
           "power"          : 'OK' if ene else 'FAIL',
           "memory"         : 'OK' if mem else 'FAIL'
           }
           
    algorithm.training_log = json.dumps(res)
    algorithm.save()
    remove_from_queue (algorithm.id)

 
  free_callback_id(callback_id)
  
  return None, 0
  



def run_evaluations ():
  
  done = True
  
  running_status   = OrderedDict()
  queued_status    = OrderedDict()
  completed_status = OrderedDict()
  failed_status    = OrderedDict()
  
  for task in TASKS_QUEUE:
  
   # print ('#########################')
   # print (task['id'])
   # print (task['type']+': '+task['kwargs']['algorithm_id'])    
   # print (task['status'])
   # print ('Waiting for:')    
   # print (task['wait_for'])
    
    if not task['type'] in running_status:
      running_status[task['type']]=0
    if not task['type'] in queued_status:
      queued_status[task['type']]=0
    if not task['type'] in completed_status:
      completed_status[task['type']]=0
    if not task['type'] in failed_status:
      failed_status[task['type']]=0
    
    if task['status']=='running':
      running_status[task['type']]+=1
    if task['status']=='queued':
      queued_status[task['type']]+=1
    if task['status']=='completed':
      completed_status[task['type']]+=1
    if task['status']=='failed':
      failed_status[task['type']]+=1
  
  print ('\n####################### STATUS ########################')
  
  
#  print (("{:10s}"+"{:^15s}"*len(queued_status)).format(" ", *queued_status.keys()))
#  print (("{:10s}"+"{:^15d}"*len(queued_status)).format("QUEUED", *queued_status.values()))
#  print (("{:10s}"+"{:^15d}"*len(running_status)).format("RUNNING", *running_status.values()))
#  print (("{:10s}"+"{:^15d}"*len(completed_status)).format("COMPLETED", *completed_status.values()))
#  print (("{:10s}"+"{:^15d}"*len(failed_status)).format("FAILED", *failed_status.values()))
  
  
  print   ("{:15s}{:^10s}{:^10s}\033[94m{:^10s}\033[0m\033[91m{:^10s}\033[0m".format("", "QUEUED", "RUNNING", "COMPLETED", "FAILED"))
  for k in queued_status.keys():
    print ("{:15s}{:^10d}{:^10d}\033[94m{:^10d}\033[0m\033[91m{:^10d}\033[0m".format(k, queued_status[k], running_status[k], completed_status[k], failed_status[k]))
  
#  print ("# Queued:")
#  print ("# ", queued_status)
#  print ("# Running:")
#  print ("# ", running_status)
#  print ("# Completed:")
#  print ("# ", completed_status)
#  print ("# Failed:")
#  print ("# ", failed_status)
  print ('#######################################################\n')
  
  evq= []
  for i, task in enumerate(TASKS_QUEUE):
    if not task['status'] == 'completed' and not task['status'] == 'failed':
      done = False
      if id_exists(task['id']): #check if exists

        ready = True
        
        w = [] 
        for cb in task['wait_for']:
          
          if id_exists(cb): #check if exists
            ready = False
            w.append(cb)
        task['wait_for'] = w
        
        if task['type'] in MAX_PARALLEL:
          max_parallel = MAX_PARALLEL[task['type']]
        else:
          max_parallel = 100
          
          
        if ready and task['status']=='queued' and running_status[task['type']]<max_parallel:
          print ("Starting")
          print (task)
          task['status']='running'
          running_status[task['type']]+=1
          
          _, job_id= task['execute'](**task['kwargs'])
          task['kwargs']['job_id']=str(job_id)
          
          if job_id == None:
            task['status']='failed'
            failed_status[task['type']]+=1
            free_callback_id(task['id'])
          else:
            task['status']='running'
            running_status[task['type']]+=1
          
      else:
        #process done
        print ("process done")
        print (task)
        status = 'completed'
        if task['run_after_exec'] is not None:
          ret = task['run_after_exec'](**task['kwargs'])
          if ret:
            status = ret
        task['status']=status
        #evq.append(i)
  
  #for i in evq:  
  #  TASKS_QUEUE.pop(i)
  
  return done
  
  
  


#   ██████╗ ██████╗ ██╗██████╗ ███████╗███████╗ █████╗ ██████╗  ██████╗██╗  ██╗
#  ██╔════╝ ██╔══██╗██║██╔══██╗██╔════╝██╔════╝██╔══██╗██╔══██╗██╔════╝██║  ██║
#  ██║  ███╗██████╔╝██║██║  ██║███████╗█████╗  ███████║██████╔╝██║     ███████║
#  ██║   ██║██╔══██╗██║██║  ██║╚════██║██╔══╝  ██╔══██║██╔══██╗██║     ██╔══██║
#  ╚██████╔╝██║  ██║██║██████╔╝███████║███████╗██║  ██║██║  ██║╚██████╗██║  ██║
#   ╚═════╝ ╚═╝  ╚═╝╚═╝╚═════╝ ╚══════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝
#                                                                              


def gridsearch_core(**kwargs):

    project_id = kwargs.get("project_id", None)
    #print (os.getcwd())
    # set up logging
    if not os.path.exists(LOGGING_FOLDER%project_id):
        os.makedirs(LOGGING_FOLDER%project_id)
    log_name = 'log_DSE_SCHED_{date:%Y_%m_%d_%H%M%S}.txt'.format( date=datetime.datetime.now())
    logging.basicConfig(level=logging.INFO,
                format="%(asctime)s - %(levelname)s - %(message)s",
                datefmt="%Y-%m-%d %H:%M:%S",
                filename = LOGGING_FOLDER%project_id + log_name,
                filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)


    send_back_status(project_id, "Running", "Job started", None, None)

    # connect to the DB
    mongo = MongoConnector()

    # retrive the project
    try:
        project = Project.objects.get(id=ObjectId(project_id))

    except Project.DoesNotExist: # if the project doesn't exist return an error
        logging.error ("ERROR! project doesn't exist")
        return
    except Project.MultipleObjectsReturned:
        logging.error ("ERROR! Multiple project")
        return


    # retrive the config
    try:
        config = ConfigurationFile.objects.get(project=project_id)

    except ConfigurationFile.DoesNotExist: # if the project doesn't exist return an error
        logging.error ("ERROR! ConfigurationFile doesn't exist")
        return
    except ConfigurationFile.MultipleObjectsReturned:
        logging.error ("ERROR! Multiple ConfigurationFile")
        return



    # retrive the ProjectConstraints
    try:
        constraints = ProjectConstraints.objects.get(project=project_id)

    except ProjectConstraints.DoesNotExist: # if the project doesn't exist return an error
        logging.error ("ERROR! ProjectConstraints doesn't exist")
        return
    except ProjectConstraints.MultipleObjectsReturned:
        logging.error ("ERROR! Multiple ProjectConstraints")
        return
        
        
    training_en        = project.tra_en.active
    power_perf_eval_en = project.per_en.active
    security_eval_en   = project.sec_en.active
    parsim_en          = project.rpi_en.active

    logging.info('################################################################################')
    logging.info('\tSTARTED. DSE Engine Scheduling started on project %s' % str(project_id))
    logging.info('################################################################################')
    #logging.info('\t Name: %s' % "name)")# str(project.name))
    #logging.info('\t Creator: %s' % str(project.creator))

    from plugins.utils.pipeline import Pipeline as PipelineClass
    from torchvision import transforms
    
    
    logging.info('You selected {} pipelines'.format(len (config.pipelines)))
    status = 'OK'
    for p in config.pipelines:
      # retrive the pipeline
      try:
          pipeline = Pipeline.objects.get(id=ObjectId(p))

      except Pipeline.DoesNotExist: # if the project doesn't exist return an error
          logging.error ("ERROR! Pipeline doesn't exist")
          return
      except Pipeline.MultipleObjectsReturned:
          logging.error ("ERROR! Multiple Pipelines")
          return
      
      # run the pipeline to get the actual input size to the net
      try:
        test_size_pipeline = PipelineClass(pipeline)
        if len(test_size_pipeline.get_transforms('sample_transforms'))>0:
          sample_transforms_composed = transforms.Compose(test_size_pipeline.get_transforms('sample_transforms'))
          sample_transforms_result = sample_transforms_composed('')
          batch_transforms_composed = transforms.Compose(test_size_pipeline.get_transforms('batch_transforms', validation=True))
          actual_input_size = (batch_transforms_composed(sample_transforms_result)).shape
          
          
          print ("actual_input_size")
          print (actual_input_size)
          
          sizes = actual_input_size
        else:
          sizes = [config.image_dimensions[2], config.image_dimensions[0], config.image_dimensions[1]]
      except:
          traceback.print_exc()

          logging.error ("ERROR! Malformed pipeline")
          return
        
        
        
      getFromFile = False
      if 'getFromFile' in config.algo_parameters:
        if config.algo_parameters['getFromFile']:
          getFromFile = True
        

      if config.algo_folder=="" or getFromFile:
        gridsearch_path = "algorithms/gridsearch_"+ str(uuid4()).split("-")[0] + "/pipeline_{}".format(p)
        os.makedirs(os.path.join(SHARED_DATA_FOLDER, gridsearch_path), exist_ok=True)

        config.algo_folder=gridsearch_path
        config.save()
        
        
        if getFromFile:
          gridsearch_file = 'gridsearch/gridsearch.json'
        else:
          json.dumps(config.algo_parameters)
          gridsearch_file = os.path.join(gridsearch_path, "gridsearch.json")
          with open(os.path.join(SHARED_DATA_FOLDER, gridsearch_file), 'w') as f: 
            f.write(json.dumps(config.algo_parameters))
        

        print ("Using file {} for Gridsearch".format(gridsearch_file))
        CH = sizes[0]
        H  = sizes[1]
        W  = sizes[2]
        
        CL = config.num_classes
        
        print ("CH: {}".format(CH))
        print ("H:  {}".format(H))
        print ("W:  {}".format(W))
        print ("CL: {}".format(CL))

        os.chdir(DSE_TOOL_PATH)

        #"java -cp ./bin:./lib/* nl.uva.aloha.GridSearchMain"
        cmd = ["java", "-cp", "./bin:./lib/*", "nl.uva.aloha.OnnxGenStandalone", os.path.join(SHARED_DATA_FOLDER, gridsearch_file),
                                                                                 os.path.join(SHARED_DATA_FOLDER, gridsearch_path),
                                                                                 str(CH), str(H), str(W), str(CL)]

        DEVNULL = os.open(os.devnull, os.O_WRONLY)
        process = subprocess.Popen(cmd, universal_newlines=True)
        #stdout=subprocess.PIPE, , stderr=DEVNULL
        from threading import Timer

        # after this timeout the process will be killed
        process_timeout=60*1000

        timer = Timer(process_timeout, process.kill)
        try:
            timer.start()
            stdout, stderr = process.communicate()
        finally:
            timer.cancel()

        result = stdout


      tests_path = config.algo_folder
      try:
        file_list  = os.listdir(os.path.join(SHARED_DATA_FOLDER, tests_path))
        file_list  = [os.path.join(tests_path, name) for name in file_list if '.onnx' in name]
      
        logging.info("Found %d ONNX files in folder %s"%(len(file_list),tests_path))
      except FileNotFoundError:
        
        logging.warning("Folder {} doesn't exists".format (tests_path))
        file_list = []
        logging.info("Found %d ONNX files in folder %s"%(len(file_list),tests_path))
      except NotADirectoryError:
        
        logging.warning("It seems you passed a file: {}".format (tests_path))
        if '.onnx' in tests_path:
          file_list = []
          file_list.append( tests_path)
        else:
          logging.warning("Model files must have .onnx extension")
          file_list = []

      status = 'OK'
     
      
      for r_eps in config.robust_loss_eps:
        for f in file_list:
          # create a new design point (algorithm, ONNX)
          algorithm_configuration = create_empty_AlgorithmConfiguration(f, project_id)
          if p != 'no_pipeline':
            algorithm_configuration.pipeline = ObjectId(p)
            
          algorithm_configuration.robust_loss_eps = r_eps
          
          algorithm_configuration.save()
          
          algorithm_id = str(algorithm_configuration.get_id())

          logging.info('################################################################################')
          logging.info('\tStarting evaluation on DSE point id %s' % str(algorithm_id))
          logging.info('################################################################################')


          logging.info('New design point created. Id: %s' % str(algorithm_id))
          logging.info('Namefile: %s' % f)
          logging.info('Pipeline: %s' % str(p))
          
          
          

          ###################### activate the tools #################

          training_en        = project.tra_en.active
          power_perf_eval_en = project.per_en.active
          security_eval_en   = project.sec_en.active
          parsim_en          = project.rpi_en.active
          
          filter_by_perf_en  = power_perf_eval_en and constraints.filter_by_perf

          print ("Training  " + ("ON" if training_en else "OFF"))
          print ("Perf eval " + ("ON" if power_perf_eval_en else "OFF"))
          print ("Security  " + ("ON" if security_eval_en else "OFF"))
          print ("RPI       " + ("ON" if parsim_en else "OFF"))

          if training_en:
              send_back_status(project_id, "Running", "Training module is waiting", "tra_en", "Waiting")
          if power_perf_eval_en:
              send_back_status(project_id, "Running", "Perf eval module is waiting", "tra_en", "Waiting")
          if security_eval_en:
              send_back_status(project_id, "Running", "Security module is waiting", "tra_en", "Waiting")
          if parsim_en:
              send_back_status(project_id, "Running", "RPI module is waiting", "tra_en", "Waiting")


          if power_perf_eval_en:
              power_perf_eval_cb = get_callback_id()

              kwargs = {'project_id'   : project_id,
                        'algorithm_id' : algorithm_id,
                        'callback_id'  : str(power_perf_eval_cb)
                        }
                        
              task = { 'id'       : str(power_perf_eval_cb),
                       'type'     : 'power perf',
                       'status'   : 'queued',
                       'wait_for' : [], 
                       'execute'  : power_perf_eval_start,
                       'kwargs'   : kwargs,
                       'run_after_exec': post_power_perf
                     }
              
              TASKS_QUEUE.append(task)

          else:
              power_perf_eval_cb=''
              logging.warning('Skipping Power Performance evaluation')
              send_back_status(project_id, "Running", "power_perf skipped", "per_en", "Skipped")

          if filter_by_perf_en:
              filter_by_perf_cb = get_callback_id()

              kwargs = {'project_id'   : project_id,
                        'algorithm'    : algorithm_configuration,
                        'algorithm_id' : algorithm_id,
                        'constraints'  : constraints,
                        'callback_id'  : str(filter_by_perf_cb)
                        }
                        
              task = { 'id'       : str(filter_by_perf_cb),
                       'type'     : 'filter_by_perf',
                       'status'   : 'queued',
                       'wait_for' : [str(power_perf_eval_cb)], 
                       'execute'  : evaluate_constraints,
                       'kwargs'   : kwargs,
                       'run_after_exec': None
                     }
              
              TASKS_QUEUE.append(task)

          else:
              filter_by_perf_cb=''


          if training_en:
              training_cb = get_callback_id()
              
              
              kwargs = {'algorithm' : algorithm_configuration,
                        'project_id'   : project_id,
                        'algorithm_id' : algorithm_id,
                        'callback_id'  : str(training_cb)
                        }
              task = { 'id'       : str(training_cb),
                       'type'     : 'training',
                       'status'   : 'queued',
                       'wait_for' : [str(filter_by_perf_cb)], 
                       'execute'  : training,
                       'kwargs'   : kwargs,
                       'run_after_exec': post_training
                     }
              
              TASKS_QUEUE.append(task)
              
              
          else:
              training_cb=''
              logging.warning('Skipping training')
              alg_path = os.path.join(SHARED_DATA_FOLDER, "experiments/prj_%s/alg_%s/onnx_storage"%(str(project_id),str(algorithm_id)))
              os.makedirs(alg_path)
              subprocess.call("cp -rf %s %s" % (os.path.join(SHARED_DATA_FOLDER,algorithm_configuration.onnx), alg_path), shell=True)
              algorithm_configuration.onnx_trained = os.path.join(alg_path.replace(SHARED_DATA_FOLDER, ''), os.path.basename(algorithm_configuration.onnx))
              algorithm_configuration.save()
              send_back_status(project_id, "Running", "training skipped", "tra_en", "Skipped")


          saveConfig_cb          = get_callback_id()
          
          kwargs = {'project_id'         : project_id,
                    'algorithm_id'       : algorithm_id,
                    'callback_id'        : str(saveConfig_cb),
                    'algorithm'          : algorithm_configuration,
                    'training_en'        : training_en
                    }
                    
          task = { 'id'       : str(saveConfig_cb),
                   'type'     : 'save_config',
                   'status'   : 'queued',
                   'wait_for' : [str(training_cb)], 
                   'execute'  : save_config,
                   'kwargs'   : kwargs,
                   'run_after_exec': None
                 }
          
          TASKS_QUEUE.append(task)

          # PARETO
          if power_perf_eval_en and training_en:
            pareto_cb = get_callback_id()

            kwargs = {'algorithm' : algorithm_configuration,
                      'algorithm_id' : algorithm_id,
                      'path'  : LOGGING_FOLDER%project_id,
                      'callback_id'        : str(pareto_cb),
                      'project_title':project.title
                      }
                      
            task = { 'id'       : str(pareto_cb),
                       'type'     : 'pareto',
                     'status'   : 'queued',
                     'wait_for' : [str(training_cb),str(power_perf_eval_cb)], 
                     'execute'  : pareto,
                     'kwargs'   : kwargs,
                     'run_after_exec': None
                   }
            
            TASKS_QUEUE.append(task)
          else:
            pareto_cb=''


          if security_eval_en:
          
              security_eval_cb   = get_callback_id()
              kwargs = {'project_id'   : project_id,
                        'algorithm_id' : algorithm_id,
                        'algorithm' : algorithm_configuration,
                        'callback_id'  : str(security_eval_cb)
                        }
                        
              task = { 'id'       : str(security_eval_cb),
                       'type'     : 'security',
                       'status'   : 'queued',
                       'wait_for' : [str(training_cb),str(saveConfig_cb)], 
                       'execute'  : security_eval,
                       'kwargs'   : kwargs,
                       'run_after_exec': post_security
                     }
              
              TASKS_QUEUE.append(task)

          else:
              security_eval_cb = ''
              logging.warning('Skipping Security evaluation')
              send_back_status(project_id, "Running", "Security evaluation skipped", "sec_en", "Skipped")



          if parsim_en:
              parsim_cb          = get_callback_id()
              
              kwargs = {'project_id'   : project_id,
                        'algorithm_id' : algorithm_id,
                        'algorithm' : algorithm_configuration,
                        'callback_id'  : str(parsim_cb)
                        }
                        
              task = { 'id'       : str(parsim_cb),
                       'type'     : 'parsim',
                       'status'   : 'queued',
                       'wait_for' : [str(training_cb),str(saveConfig_cb)], 
                       'execute'  : parsim_start,
                       'kwargs'   : kwargs,
                       'run_after_exec': post_parsim
                     }
              
              TASKS_QUEUE.append(task)
                        
              
              
          else:
              parsim_cb=''
              logging.warning('Skipping Refinement for parsimonious inference')
              send_back_status(project_id, "Running", "Refinement for parsimonious inference skipped", "rpi_en", "Skipped")



          complete_cb          = get_callback_id()
          
          kwargs = {'project_id'         : project_id,
                    'algorithm_id'       : algorithm_id,
                    'callback_id'        : str(complete_cb),
                    'algorithm'          : algorithm_configuration,
                    'training_en'        : training_en,
                    'power_perf_eval_en' : power_perf_eval_en
                    }
                    
          task = { 'id'       : str(complete_cb),
                   'type'     : 'finalize',
                   'status'   : 'queued',
                   'wait_for' : [str(training_cb), str(power_perf_eval_cb), str(pareto_cb), str(security_eval_cb), str(parsim_cb),str(saveConfig_cb)], 
                   'execute'  : evaluation_complete,
                   'kwargs'   : kwargs,
                   'run_after_exec': None
                 }
          
          TASKS_QUEUE.append(task)

    done = False
    while not done:
      done = run_evaluations()
      time.sleep(10)

    TASKS_QUEUE.clear()

# iteration over ONNXs completed


    if status=='OK':
        data={
        "status":"Finished",
        "log": "nothing"
        }

    else:
        data={
        "status":"Errors",
        "log": "nothing"
        }
    logging.info('################################################################################')
    logging.info('\t{}. DSE Engine Scheduling finished on project {}'.format("COMPLETED" if status=='OK' else "FAILED", str(project_id)))
    logging.info('################################################################################')


    logging.info('Sending status to frontend')
   
    url='http://orchestrator_be:5000/api/projects/%s/status'%(str(project_id))


    logging.debug("Request URL %s" % url)
    logging.debug("Request data %s" % str(data))

    r = requests.post(url, json=data)

    logging.info("Response status code %s" % r.status_code )







#   ██████╗  █████╗ 
#  ██╔════╝ ██╔══██╗
#  ██║  ███╗███████║
#  ██║   ██║██╔══██║
#  ╚██████╔╝██║  ██║
#   ╚═════╝ ╚═╝  ╚═╝
#                   




def ga_core(**kwargs):

    project_id = kwargs.get("project_id", None)


    # set up logging
    if not os.path.exists(LOGGING_FOLDER%project_id):
        os.makedirs(LOGGING_FOLDER%project_id)
    log_name = 'log_DSE_SCHED_{date:%Y_%m_%d_%H%M%S}.txt'.format( date=datetime.datetime.now())
    logging.basicConfig(level=logging.INFO,
                format="%(asctime)s - %(levelname)s - %(message)s",
                datefmt="%Y-%m-%d %H:%M:%S",
                filename = LOGGING_FOLDER%project_id + log_name,
                filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)


    # connect to the DB
    mongo = MongoConnector()


    # retrive the project
    try:
        project = Project.objects.get(id=ObjectId(project_id))

    except Project.DoesNotExist: # if the project doesn't exist return an error
        logging.error ("ERROR! project doesn't exist")
        return
    except Project.MultipleObjectsReturned:
        logging.error ("ERROR! Multiple project")
        return

    training_en        = project.tra_en.active
    power_perf_eval_en = project.per_en.active
    security_eval_en   = project.sec_en.active
    parsim_en          = project.rpi_en.active

    logging.info('################################################################################')
    logging.info('\tSTARTED. J-DSE Engine started on project %s' % str(project_id))
    logging.info('################################################################################')
    #logging.info('\t Name: %s' % "name)")# str(project.name))
    #logging.info('\t Creator: %s' % str(project.creator))


    DSE_TOOL_PATH = "/opt/www/modules/ga_aloha/"
    os.chdir(DSE_TOOL_PATH)

    #"java -cp ./bin:./lib/* nl.uva.aloha.MultiObjGAMain"
    cmd = ["java", "-cp", "./bin:./lib/*", "nl.uva.aloha.MultiObjGAMain", project_id, SHARED_DATA_FOLDER]

    DEVNULL = os.open(os.devnull, os.O_WRONLY)
    process = subprocess.Popen(cmd, universal_newlines=True)
 
    redisc=RedisConnector()
    
    # lock for mutual exclusion in case of multithreading (not used for multiple workers)
    lock = redis_lock.Lock(redisc.conn, "GA_DSE_engine_LOCK")

    
    GA_label= str(project_id)+":GAONNX:"
    done = False
    while not done:
     
      run_evaluations()
     
      got_key = False

      lock.acquire(blocking=True) #acquire the lock
      #print ("lock acquired")
      
    
      path_keys = redisc.conn.keys(pattern=GA_label+"path"+"*") 
      done_key  = redisc.conn.keys(pattern=GA_label+"done"+"*")
      
      print ("path keys: {}".format(len(path_keys)))
      
      if len(done_key)>0:
        print ("done")
        done    = True
        got_key = True
        redisc.conn.delete(done_key[0])
        lock.release() 
        
      elif len(path_keys)==0:
        lock.release() # if no model is ready release the lock
        time.sleep(10)
      else:
        got_key = True
        key = path_keys[0].decode('utf-8')
        
        logging.info('\tGot key: {}'.format (key))
        
        #redis get onnx path
        path = redisc.conn.get(key).decode('utf-8')
        
        logging.info('\tOnnx path: {}'.format (path))
        # delete key
        redisc.conn.delete(key)
        
        lock.release() #release the lock after the key is consumed
      
      if done == False and got_key:
        #add onnx to db

        algorithm_configuration = create_empty_AlgorithmConfiguration(path, project_id)
        
        algorithm_id = algorithm_configuration.get_id()
        logging.info('################################################################################')
        logging.info('\tStarting evaluation on DSE point id %s' % str(algorithm_id))
        logging.info('################################################################################')

        
        # start evaluation over the onnx
        
        #status = add_DSP_evaluation(project, algorithm_configuration, key)
        
        
        
        
        
        

        ###################### activate the tools #################

        training_en        = project.tra_en.active
        power_perf_eval_en = project.per_en.active
        security_eval_en   = project.sec_en.active
        parsim_en          = project.rpi_en.active

        print ("Training  " + ("ON" if training_en else "OFF"))
        print ("Perf eval " + ("ON" if power_perf_eval_en else "OFF"))
        print ("Security  " + ("ON" if security_eval_en else "OFF"))
        print ("RPI       " + ("ON" if parsim_en else "OFF"))

        if training_en:
            send_back_status(project_id, "Running", "Training module is waiting", "tra_en", "Waiting")
        if power_perf_eval_en:
            send_back_status(project_id, "Running", "Perf eval module is waiting", "tra_en", "Waiting")
        if security_eval_en:
            send_back_status(project_id, "Running", "Security module is waiting", "tra_en", "Waiting")
        if parsim_en:
            send_back_status(project_id, "Running", "RPI module is waiting", "tra_en", "Waiting")




        if training_en:
            training_cb = get_callback_id()
            
            
            kwargs = {'project_id'   : project_id,
                      'algorithm_id' : algorithm_id,
                      'callback_id'  : str(training_cb)
                      }
            task = { 'id'       : str(training_cb),
                     'type'     : 'training',
                     'status'   : 'queued',
                     'wait_for' : [], 
                     'execute'  : training,
                     'kwargs'   : kwargs,
                     'run_after_exec': None
                   }
            
            TASKS_QUEUE.append(task)
            
            
        else:
            logging.warning('Skipping training')
            alg_path = os.path.join(SHARED_DATA_FOLDER, "experiments/prj_%s/alg_%s/onnx_storage"%(str(project_id),str(algorithm_id)))
            os.makedirs(alg_path)
            subprocess.call("cp -rf %s %s" % (os.path.join(SHARED_DATA_FOLDER,algorithm_configuration.onnx), alg_path), shell=True)
            algorithm_configuration.onnx_trained = os.path.join(alg_path.replace(SHARED_DATA_FOLDER, ''), os.path.basename(algorithm_configuration.onnx))
            algorithm_configuration.save()
            send_back_status(project_id, "Running", "training skipped", "tra_en", "Skipped")




        if power_perf_eval_en:
            power_perf_eval_cb = get_callback_id()

            kwargs = {'project_id'   : project_id,
                      'algorithm_id' : algorithm_id,
                      'callback_id'  : str(power_perf_eval_cb)
                      }
                      
            task = { 'id'       : str(power_perf_eval_cb),
                     'type'     : 'power_perf',
                     'status'   : 'queued',
                     'wait_for' : [], 
                     'execute'  : power_perf_eval_start,
                     'kwargs'   : kwargs,
                     'run_after_exec': None
                   }
            
            TASKS_QUEUE.append(task)

        else:
            power_perf_eval_cb=''
            logging.warning('Skipping Power Performance evaluation')
            send_back_status(project_id, "Running", "power_perf skipped", "per_en", "Skipped")



        # PARETO
        if power_perf_eval_en and training_en:
          pareto_cb = get_callback_id()

          kwargs = {'algorithm' : algorithm_configuration,
                    'algorithm_id' : algorithm_id,
                    'path'  : LOGGING_FOLDER%project_id,
                    'callback_id'        : str(pareto_cb),
                    'project_title':project.title
                    }
                    
          task = { 'id'       : str(pareto_cb),
                     'type'     : 'pareto',
                   'status'   : 'queued',
                   'wait_for' : [str(training_cb),str(power_perf_eval_cb)], 
                   'execute'  : pareto,
                   'kwargs'   : kwargs,
                   'run_after_exec': None
                 }
          
          TASKS_QUEUE.append(task)
        


        if security_eval_en:
        
            security_eval_cb   = get_callback_id()
            kwargs = {'project_id'   : project_id,
                      'algorithm_id' : algorithm_id,
                      'algorithm' : algorithm_configuration,
                      'callback_id'  : str(security_eval_cb)
                      }
                      
            task = { 'id'       : str(security_eval_cb),
                     'type'     : 'security',
                     'status'   : 'queued',
                     'wait_for' : [str(training_cb)], 
                     'execute'  : security_eval,
                     'kwargs'   : kwargs,
                     'run_after_exec': post_security
                   }
            
            TASKS_QUEUE.append(task)

        else:
            security_eval_cb = ''
            logging.warning('Skipping Security evaluation')
            send_back_status(project_id, "Running", "Security evaluation skipped", "sec_en", "Skipped")



        if parsim_en:
            parsim_cb          = get_callback_id()
            
            kwargs = {'project_id'   : project_id,
                      'algorithm_id' : algorithm_id,
                      'algorithm' : algorithm_configuration,
                      'callback_id'  : str(parsim_cb)
                      }
                      
            task = { 'id'       : str(parsim_cb),
                     'type'     : 'parsim',
                     'status'   : 'queued',
                     'wait_for' : [str(training_cb)], 
                     'execute'  : parsim_start,
                     'kwargs'   : kwargs,
                     'run_after_exec': post_parsim
                   }
            
            TASKS_QUEUE.append(task)
                      
            
            
        else:
            parsim_cb=''
            logging.warning('Skipping Refinement for parsimonious inference')
            send_back_status(project_id, "Running", "Refinement for parsimonious inference skipped", "rpi_en", "Skipped")


        sendGA_cb          = get_callback_id()
        
        kwargs = {'project_id'         : project_id,
                  'algorithm_id'       : algorithm_id,
                  'callback_id'        : str(sendGA_cb),
                  'algorithm'          : algorithm_configuration,
                  'key'                : key
                  }
                  
        task = { 'id'       : str(sendGA_cb),
                 'type'     : 'send_GA',
                 'status'   : 'queued',
                 'wait_for' : [str(training_cb), str(power_perf_eval_cb), str(pareto_cb), str(security_eval_cb), str(parsim_cb)], 
                 'execute'  : sendGA,
                 'kwargs'   : kwargs,
                 'run_after_exec': None
               }
        
        TASKS_QUEUE.append(task)



        complete_cb          = get_callback_id()
        
        kwargs = {'project_id'         : project_id,
                  'algorithm_id'       : algorithm_id,
                  'callback_id'        : str(complete_cb),
                  'algorithm'          : algorithm_configuration,
                  'training_en'        : training_en,
                  'power_perf_eval_en' : power_perf_eval_en
                  }
                  
        task = { 'id'       : str(complete_cb),
                 'type'     : 'finalize',
                 'status'   : 'queued',
                 'wait_for' : [str(training_cb), str(power_perf_eval_cb), str(pareto_cb), str(security_eval_cb), str(parsim_cb),str(sendGA_cb)], 
                 'execute'  : evaluation_complete,
                 'kwargs'   : kwargs,
                 'run_after_exec': None
               }
        
        TASKS_QUEUE.append(task)

        

    try:
        stdout, stderr = process.communicate()
    except:
        pass

    TASKS_QUEUE.clear()
    
    status = 'OK'

    if status=='OK':
        data={
        "status":"Finished",
        "log": "nothing"
        }

    else:
        data={
        "status":"Errors",
        "log": "nothing"
        }
    logging.info('################################################################################')
    logging.info('\{}. DSE Engine Scheduling finished on project {}'.format("COMPLETED" if status=='OK' else "FAILED", str(project_id)))
    logging.info('################################################################################')


    logging.info('Sending status to frontend')
   
    url='http://orchestrator_be:5000/api/projects/%s/status'%(str(project_id))


    logging.debug("Request URL %s" % url)
    logging.debug("Request data %s" % str(data))

    r = requests.post(url, json=data)

    logging.info("Response status code %s" % r.status_code )





# █████╗ ██████╗ ██╗
#██╔══██╗██╔══██╗██║
#███████║██████╔╝██║
#██╔══██║██╔═══╝ ██║
#██║  ██║██║     ██║
#╚═╝  ╚═╝╚═╝     ╚═╝
#

class DseEngine(Resource):

    def post(self):
        project_id = g.args.get ("project_id")
        #print (os.getcwd())
        # set up logging
        if not os.path.exists(LOGGING_FOLDER%project_id):
            os.makedirs(LOGGING_FOLDER%project_id)
        log_name = 'log_DSE_API_{date:%Y_%m_%d_%H%M%S}.txt'.format( date=datetime.datetime.now())
        logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s - %(levelname)s - %(message)s",
                    datefmt="%Y-%m-%d %H:%M:%S",
                    filename = LOGGING_FOLDER%project_id + log_name,
                    filemode='w')
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                      datefmt='%Y-%m-%d %H:%M:%S')
        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)


        logging.info('################################################################################')
        logging.info('\tDSE Engine received a new job on project %s' % str(project_id))
        logging.info('################################################################################')
        try:
          prjID  = ObjectId(g.args.get ("project_id"))
        except: # check the id, it must be a 24digit hex number
          logging.error ("ERROR! Wrong id format")
          return {"message": "ERROR! Wrong id format. It must be a 24digit hex number", "code": 400}, 400, {}


        try:
          project   = Project.objects.get(id=prjID)

        except Project.DoesNotExist:
          logging.error ("ERROR! The project doesn't exist")
          return {"message": "ERROR! The project doesn't exist", "code": 410}, 410, {}
 
 
      # retrive the config and save the json file
        try:
            config = ConfigurationFile.objects.get(project=str(prjID))
            filenamejson = LOGGING_FOLDER%project_id + "ConfigurationFile.json"
            config_str = config.to_json().replace(', "', ',\n"')
            with open(filenamejson, 'w') as f: 
              f.write(config_str) 
            print (config_str)

        except ConfigurationFile.DoesNotExist: # if the project doesn't exist return an error
            logging.error ("ERROR! ConfigurationFile doesn't exist")
            return
        except ConfigurationFile.MultipleObjectsReturned:
            logging.error ("ERROR! Multiple ConfigurationFile")
            return


        SCHEDULER = project.dse_core
        dse_job_timeout = os.getenv('DSEJOB_TIMEOUT', 60*60*24*15) # default value is 15 days. -1 is an infinite timeout
        

        with Connection(conn): # connection to REDIS DB
          q = Queue("dse",connection=conn)


          queued_job_ids = q.job_ids

          # enqueue a new job

          if SCHEDULER == "GRIDSEARCH":
              logging.info('Using Gridsearch core')
              job = q.enqueue_call(func=gridsearch_core, result_ttl=600000, timeout=dse_job_timeout, kwargs={"project_id":str(prjID)})
          elif SCHEDULER == "GA":
              logging.info('Using Genetic Algorithm core')
              job = q.enqueue_call(func=ga_core, result_ttl=600000, timeout=dse_job_timeout, kwargs={"project_id":str(prjID)})


          if len(queued_job_ids)>0:
            logging.info('Job queued: %d'%len(queued_job_ids))
            # Do not make a call to the orchestrator. This cause a deadlock, the orchestrator cannot answer the call until this function doesn't return
            #send_back_status (project_id, "Queued", "Job queued. %d jobs in queue"%len(queued_job_ids))
            return {"message": "queued", "job_id": job.id}, 200, None

        return {"message": "started", "job_id": job.id}, 200, None
