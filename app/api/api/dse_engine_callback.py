# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import request, g

from . import Resource
from .. import schemas

from worker import *

class DseEngineCallback(Resource):

    def post(self):
        print(g.args)
        print("Callback received")

        callback_id = str(g.args.get ("callback_id"))
        if conn.exists(callback_id):
            print("Callback_id exists")
        else:
            print ("[WARNING] callback_id doesn't exist")
            return {"resp": "WARNING! The callback_id doesn't exist"}, 204, {}

        conn.delete(callback_id)

        return {'resp': 'Callback received'}, 200, None
