# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import request, g

from . import Resource
from .. import schemas

from worker import *
from rq.job import Job

from api.api.dse_engine import send_back_status

class DseEngineFailed(Resource):

    def post(self):
        #print(g.args)
        print("Job failed received")
        job_id = request.args.get("job_id")
        print(job_id)
        data= request.form.to_dict()
        print(data)
        ## GET project
        job = Job.fetch(job_id, connection=conn)
        print(job.kwargs)


        if job.get_status() == "failed":
            print("Changing DB status for failed job")
            send_back_status(job.kwargs["project_id"], "errors", str("%s failed" % data['module']), data['module'], "Failed")
        else:
            print ("ERROR: Job doesn't exist")
            return {"message": "ERROR! The callback_id doesn't exist", "code": 410}, 410, {}

        conn.delete(job_id)

        return {'resp': 'Job failed received'}, 200, None
