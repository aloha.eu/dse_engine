# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import Flask, request, g

from . import Resource
from .. import schemas

# the following two imports are needed for create a new connection
from settings import settings
from db.mongo import MongoConnector

from rq import Queue
from worker import *

from models.models import *

from bson import ObjectId

from uuid import uuid4

import requests


#  ██████╗ ███████╗██████╗ ███████╗    ███████╗██╗   ██╗ █████╗ ██╗         ██╗    ██╗██████╗  █████╗ ██████╗
#  ██╔══██╗██╔════╝██╔══██╗██╔════╝    ██╔════╝██║   ██║██╔══██╗██║         ██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  ██████╔╝█████╗  ██████╔╝█████╗      █████╗  ██║   ██║███████║██║         ██║ █╗ ██║██████╔╝███████║██████╔╝
#  ██╔═══╝ ██╔══╝  ██╔══██╗██╔══╝      ██╔══╝  ╚██╗ ██╔╝██╔══██║██║         ██║███╗██║██╔══██╗██╔══██║██╔═══╝
#  ██║     ███████╗██║  ██║██║         ███████╗ ╚████╔╝ ██║  ██║███████╗    ╚███╔███╔╝██║  ██║██║  ██║██║
#  ╚═╝     ╚══════╝╚═╝  ╚═╝╚═╝         ╚══════╝  ╚═══╝  ╚═╝  ╚═╝╚══════╝     ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝

def power_perf_eval (**kwargs):



  host                       = kwargs.get("host", None)
  project_id                 = kwargs.get("projectID", None)
  algorithm_configuration_id = kwargs.get("algorithm_configuration_id", None)
  architecture_id            = kwargs.get("architecture_id", None)


  #pow_per= ToolWrapper('power_perf_eval',host)

  # start power performance evaluation
  api='http://%s/api/eval_perf?algorithm_configuration_id=%s&project_id=%s&architecture_id=%s'%(host,algorithm_configuration_id, project_id, architecture_id)
  r = requests.post(api)

  print(r)
  #job_id=r.json()['job_id']





  #wait for results
  #api = 'http://%s/api/eval_perf/status?job_id=%s'%(host, job_id)

  import time
  done = 0
  while done==0:
    r=requests.get(api)
    if r.json()['message']=='queued' or r.json()['message']=='started':
      time.sleep(5)
    else:
      done = 1

  print(r.json())



  #get results
  if r.json()['message']== 'finished':
    api='http://%s/api/eval_perf?algorithm_configuration_id=%s&project_id=%s&architecture_id=%s'%(host,algorithm_configuration_id, project_id, architecture_id)

    r = requests.get(api)

    print(r.json())
  elif r.json()['message']== 'failed':
    return 'failed'

  return 'OK'



#  ██████╗  █████╗ ██████╗ ███████╗██╗███╗   ███╗    ██╗███╗   ██╗███████╗███████╗    ██╗    ██╗██████╗  █████╗ ██████╗
#  ██╔══██╗██╔══██╗██╔══██╗██╔════╝██║████╗ ████║    ██║████╗  ██║██╔════╝██╔════╝    ██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  ██████╔╝███████║██████╔╝███████╗██║██╔████╔██║    ██║██╔██╗ ██║█████╗  █████╗      ██║ █╗ ██║██████╔╝███████║██████╔╝
#  ██╔═══╝ ██╔══██║██╔══██╗╚════██║██║██║╚██╔╝██║    ██║██║╚██╗██║██╔══╝  ██╔══╝      ██║███╗██║██╔══██╗██╔══██║██╔═══╝
#  ██║     ██║  ██║██║  ██║███████║██║██║ ╚═╝ ██║    ██║██║ ╚████║██║     ███████╗    ╚███╔███╔╝██║  ██║██║  ██║██║
#  ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝╚═╝     ╚═╝    ╚═╝╚═╝  ╚═══╝╚═╝     ╚══════╝     ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝
#


def parsim (**kwargs):



  host                       = kwargs.get("host", None)
  project_id                 = kwargs.get("projectID", None)
  algorithm_configuration_id = kwargs.get("algorithm_configuration_id", None)
  architecture_id            = kwargs.get("architecture_id", None)


  #pow_per= ToolWrapper('power_perf_eval',host)
  callback_id = uuid4()
  conn.set(callback_id, '1')

  # start power performance evaluation
  url='http://%s/api/rpi?callback_id=%s'%(host,callback_id)
  data={"project_id":project_id,
        "algorithm_configuration_id":algorithm_configuration_id,
        "method":"string",
        "network":"string",
        "method_params": 0}

  r = requests.post(url,data=data)

  print(r.json())
  job_id=r.json()['job_id']



  #wait for results. Query the redis DB for the exisistence of the callback_id key
  import time
  done = 0
  while done==0:
    if conn.exists(callback_id):
      time.sleep(5)
    else:
      done = 1



  #get results
  url='http://%s/api/rpi/{%s}?'%(host,job_id)

  r = requests.get(url)



  return 'OK'








# ████████╗██████╗  █████╗ ██╗███╗   ██╗██╗███╗   ██╗ ██████╗     ██╗    ██╗██████╗  █████╗ ██████╗
# ╚══██╔══╝██╔══██╗██╔══██╗██║████╗  ██║██║████╗  ██║██╔════╝     ██║    ██║██╔══██╗██╔══██╗██╔══██╗
#    ██║   ██████╔╝███████║██║██╔██╗ ██║██║██╔██╗ ██║██║  ███╗    ██║ █╗ ██║██████╔╝███████║██████╔╝
#    ██║   ██╔══██╗██╔══██║██║██║╚██╗██║██║██║╚██╗██║██║   ██║    ██║███╗██║██╔══██╗██╔══██║██╔═══╝
#    ██║   ██║  ██║██║  ██║██║██║ ╚████║██║██║ ╚████║╚██████╔╝    ╚███╔███╔╝██║  ██║██║  ██║██║
#    ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝╚═╝╚═╝  ╚═══╝ ╚═════╝      ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝


def training (**kwargs):



  host                       = kwargs.get("host", None)
  project_id                 = kwargs.get("projectID", None)
  algorithm_configuration_id = kwargs.get("algorithm_configuration_id", None)
  architecture_id            = kwargs.get("architecture_id", None)



  # start a new training

  api='http://%s/api/training_call?projectId=%s&jobId=%s'%(host, project_id, algorithm_configuration_id )
 # api='http://%s/api/eval_perf?algorithm_configuration_id=%s&project_id=%s&architecture_id=%s'%(host,algorithm_configuration_id, project_id, architecture_id)
  r = requests.post(api)


  print (r.status_code)

  try:
    print (r.json())
  except:
    print ("No JSON returned")

  #job_id=r.json()['job_id']



  return 'OK'





#  ███████╗███████╗ ██████╗██╗   ██╗██████╗ ██╗████████╗██╗   ██╗    ███████╗██╗   ██╗ █████╗ ██╗
#  ██╔════╝██╔════╝██╔════╝██║   ██║██╔══██╗██║╚══██╔══╝╚██╗ ██╔╝    ██╔════╝██║   ██║██╔══██╗██║
#  ███████╗█████╗  ██║     ██║   ██║██████╔╝██║   ██║    ╚████╔╝     █████╗  ██║   ██║███████║██║
#  ╚════██║██╔══╝  ██║     ██║   ██║██╔══██╗██║   ██║     ╚██╔╝      ██╔══╝  ╚██╗ ██╔╝██╔══██║██║
#  ███████║███████╗╚██████╗╚██████╔╝██║  ██║██║   ██║      ██║       ███████╗ ╚████╔╝ ██║  ██║███████╗
#  ╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝   ╚═╝      ╚═╝       ╚══════╝  ╚═══╝  ╚═╝  ╚═╝╚══════╝
#



def security_eval (**kwargs):



  host                       = kwargs.get("host", None)
  project_id                 = kwargs.get("projectID", None)
  algorithm_configuration_id = kwargs.get("algorithm_configuration_id", None)
  architecture_id            = kwargs.get("architecture_id", None)
  dataset_id                 = kwargs.get("dataset_id", None)


  req_body = {
  "dataset": dataset_id,
  "trained-model": algorithm_configuration_id,
  "performance-metric": "classification-accuracy",
  "perturbation-type": "max-norm",
  "perturbation-values": [0, 1, 2, 5, 10]
  }

  # start Security evaluation
  url = 'http://%s/api/security_evaluations' % (host)
  r = requests.post(url, data=req_body)

  print (r.status_code)

  job_id=r.json()['job_id']
  #wait for results


  api = 'http://%s/api/security_evaluations/{%s}'%(host, job_id)

  import time
  done = 0
  while done==0:
    r=requests.get(api)
    if r.status_code==200:
      time.sleep(10)
    else:
      done = 1


  print(r.json())

  return 'OK'





def schedule_processes(**kwargs):


  port=5000

  project_id = kwargs.get("projectID", None)

  print (project_id)

  # connect to the DB
  app = Flask(__name__, static_folder='static')
  settings.get_settings(app)

  mongo = MongoConnector(app)


  #architecture = Architecture.objects.get(project=project_id)
  # retrive the architecture
  try:
    architecture = Architecture.objects.get(project=str(project_id))

    print (repr(architecture))
  except Architecture.DoesNotExist: # if the project doesn't exist return an error
    print ("ERROR! Architecture doesn't exist")
    return
  except Architecture.MultipleObjectsReturned:
    print ("ERROR! Multiple architectures associated to the same project")
    return


  # retrive the dataset
  try:
    dataset = Dataset.objects.get(project=str(project_id))

    print (repr(dataset))
  except Dataset.DoesNotExist: # if the project doesn't exist return an error
    print ("ERROR! Architecture doesn't exist")
    return
  except Dataset.MultipleObjectsReturned:
    print ("ERROR! Multiple architectures associated to the same project")
    return

    # create a new design point (algorithm, ONNX)
  algorithm_configuration = AlgorithmConfiguration()
  algorithm_configuration.project = str(project_id)
  algorithm_configuration.path = "path/to/the/new/file"
  algorithm_configuration.save()

    # print(algorithm_configuration.path)

  architecture_id = str(architecture.get_id())







  status='OK'

  '''
  print ("Starting Power Performance evaluation")
  status = power_perf_eval(host="power_perf:%d"%port, algorithm_configuration_id=algorithm_configuration.get_id(), architecture_id=architecture_id, **kwargs)
  print ("Power Performance evaluation started. Status:")
  print (status)
  '''


  print ("Starting Security evaluation")
  #status = security_eval(host="security:%d"%port, dataset=dataset.get_id(), algorithm_configuration_id=algorithm_configuration.get_id(), architecture_id=architecture_id, **kwargs)
  print ("Security evaluation started. Status:")
  print (status)


  '''
  training_id = uuid4();
  conn.set(training_id,1)
  #a=conn.keys(pattern='*')
  print (a)
  print ("Starting training")
  status = training(host="training:%d"%port, algorithm_configuration_id=algorithm_configuration.get_id(), architecture_id=architecture_id, **kwargs)
  print ("Training done. Status:")
  print (status)
  '''


  #status = parsim(host="localhost:%d"%port, algorithm_configuration_id=algorithm_configuration.get_id(), architecture_id=architecture_id, **kwargs)

  print ("Starting parsim")
  status = parsim(host="parsim:%d"%port, algorithm_configuration_id=algorithm_configuration.get_id(), architecture_id=architecture_id, **kwargs)
  print ("parsim done. Status:")




  return status




class StartProcess(Resource):

    def post(self):
        #print(g.args)
        #print (Project.objects.count())

        try:
          prjID  = ObjectId(g.args.get ("project_id"))
        except: # check the id, it must be a 24digit hex number
          print ("ERROR! Wrong id format")
          return {"message": "ERROR! Wrong id format. It must be a 24digit hex number", "code": 400}, 400, {}


        try:
          project   = Project.objects.get(id=prjID)

          print (repr(project))
        except Project.DoesNotExist: # if the project doesn't exist return an error
          print ("ERROR! The project doesn't exist")
          return {"message": "ERROR! The project doesn't exist", "code": 410}, 410, {}

        with Connection(conn): # connection to REDIS DB
          q = Queue("dse",connection=conn)

          # enqueue a new job
          job = q.enqueue_call(func=schedule_processes, result_ttl=600000, timeout=600000, kwargs={"projectID":str(prjID)})



        elabID = job.id;


        return {"message": "Elaboration started", "job_id": elabID}, 200, None
