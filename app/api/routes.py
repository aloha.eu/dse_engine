# -*- coding: utf-8 -*-

###
### DO NOT CHANGE THIS FILE
###
### The code is auto generated, your change will be overwritten by
### code generating.
###
from __future__ import absolute_import

from .api.dse_engine_status import DseEngineStatus
from .api.dse_engine_callback import DseEngineCallback
from .api.dse_engine_failed import DseEngineFailed
from .api.dse_engine import DseEngine


routes = [
    dict(resource=DseEngineStatus, urls=['/dse_engine/status'], endpoint='dse_engine_status'),
    dict(resource=DseEngineCallback, urls=['/dse_engine/callback'], endpoint='dse_engine_callback'),
    dict(resource=DseEngineFailed, urls=['/dse_engine/failed'], endpoint='dse_engine_failed'),
    dict(resource=DseEngine, urls=['/dse_engine'], endpoint='dse_engine'),
]
