#!/usr/bin/python3
import os
print ("Dropping DB!!!")

os.environ["MONGO_DB_TCP_ADDR"] = "localhost"
os.environ["MONGO_DB_NAME"] = "aloha"
os.environ["MONGO_DB_PORT"] = "27018"

from models.models import *
from db.mongo import MongoConnector
mongo = MongoConnector()


mongo.db.drop_collection('User')
mongo.db.drop_collection('Dataset')
mongo.db.drop_collection('Project')
mongo.db.drop_collection('StatusTE')
mongo.db.drop_collection('AlgorithmConfiguration')
mongo.db.drop_collection('ConfigurationFile')

