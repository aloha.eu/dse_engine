import logging
import os

from redis import Redis
from rq import Worker, Queue, Connection


# use rq-dashboard for visualization

class RedisConnector():

    def __init__(self):
        # todo improve this
        logging.debug ("Connecting to REDISDB: ")
        logging.debug ('\t'+str(os.environ['REDIS_HOST']))
        logging.debug ('\t'+str(os.environ['REDIS_PORT']))
        logging.debug ('\t'+str(os.environ['REDIS_QUEUE']))
        
        listen = [os.environ.get('REDIS_QUEUE', 'default')]

        REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')
        REDIS_PORT = os.environ.get('REDIS_PORT', '6379')
        
        try:
            self.conn = Redis(host=REDIS_HOST, port=REDIS_PORT)
            logging.debug ("Connected!")
        except Exception as e:
            logging.error("Connection error")
            logging.debug(e)
            
            
