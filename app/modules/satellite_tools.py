# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function


# the following two imports are needed for create a new connection

#from db.mongo import MongoConnector

#from rq import Queue
#from worker import *

#from models.models import *

from bson import ObjectId

from uuid import uuid4

import requests

import os
import traceback

import logging

from db.redis import RedisConnector
from rq.job import Job

from models.models import AlgorithmConfiguration, Dataset, Project, Architecture, ConfigurationFile


#from .redisConnector import RedisConnector

CB_WAIT_TIME = 5
SHARED_DATA_FOLDER = "/opt/data/"
LOGGING_FOLDER = os.path.join(SHARED_DATA_FOLDER, "experiments/prj_%s/dse_engine_logs/") # to be completed with the project_id



def send_back_status(project_id, status, log, current, step_status, **kwargs):
    if current:
        data={
            "status":status,
            "log": log,
            "info": {"type":"dse_engine",
                    "step" : current, #if current training, security, rpi or performance. then takes the status from the "status" above - If process starts all the indicators should be set to Ready, if their are disabled, should be set to "disabled".
                    "step_status" : step_status
                    }
          }
    else:
        data={
          "status":status,
          "log": log
        }

    logging.info('Sending status to orchestrator')
    url='http://orchestrator_be:5000/api/projects/%s/status'%(str(project_id))
    logging.debug("Request URL %s" % url)
    logging.debug("Request data %s" % str(data))

    r = requests.post(url, json=data)

    logging.info("Response status code %s" % r.status_code )


def free_callback_id(callback_id):
    redisc=RedisConnector()
    
    redisc.conn.delete(callback_id)
    print ("Deleted callback_id: %s"%str(callback_id))

def barrier_cb (callback_id):
   # print ("barrier")
    redisc=RedisConnector()
    logging.info ("Waiting for callback (id: %s)" % callback_id)
    #wait for results. Query the redis DB for the exisistence of the callback_id key
    import time
    done = 0
    while done==0:
      if redisc.conn.exists(callback_id):
        #print ("Waiting for callback (id: %s)" % callback_id)
        time.sleep(CB_WAIT_TIME)
      else:
        done = 1

    logging.info ("Callback caugth.")

def id_exists (callback_id):

    redisc=RedisConnector()
    return redisc.conn.exists(callback_id)
    


def get_callback_id ():
    #print ("get")
    callback_id = uuid4()
    redisc=RedisConnector()
  #  print (callback_id)

    #print (redisc.conn.set(str(callback_id), '1'))
    #print ("get3")
    try:
        redisc.conn.set(str(callback_id), '1')
        print ("Callback_id: %s"%str(callback_id))
    except:
        logging.error ("ERROR: Unable to set a callback id in the REDIS DB.")

    #import time
    #time.sleep(30)
    return callback_id





#  ██████╗ ███████╗██████╗ ███████╗    ███████╗██╗   ██╗ █████╗ ██╗         ██╗    ██╗██████╗  █████╗ ██████╗
#  ██╔══██╗██╔════╝██╔══██╗██╔════╝    ██╔════╝██║   ██║██╔══██╗██║         ██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  ██████╔╝█████╗  ██████╔╝█████╗      █████╗  ██║   ██║███████║██║         ██║ █╗ ██║██████╔╝███████║██████╔╝
#  ██╔═══╝ ██╔══╝  ██╔══██╗██╔══╝      ██╔══╝  ╚██╗ ██╔╝██╔══██║██║         ██║███╗██║██╔══██╗██╔══██║██╔═══╝
#  ██║     ███████╗██║  ██║██║         ███████╗ ╚████╔╝ ██║  ██║███████╗    ╚███╔███╔╝██║  ██║██║  ██║██║
#  ╚═╝     ╚══════╝╚═╝  ╚═╝╚═╝         ╚══════╝  ╚═══╝  ╚═╝  ╚═╝╚══════╝     ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝

def power_perf_eval_start (project_id=None, algorithm_id=None, callback_id=None, **kwargs):

    host            = "power_perf:5000"
   # project_id      = kwargs.get("projectID", None)
   # algorithm_id    = kwargs.get("algorithm_id", None)


    logging.info('Power Performance evaluation started.')
    send_back_status(project_id, "Running", "Power Performance evaluation started", "per_en", "Running")
    #get a callback id
    if callback_id==None:
      callback_id = get_callback_id()

    url='http://%s/api/power_performance_evaluations?callback_id=%s'%(host,callback_id)
    data={
    "project_id":str(project_id),
    "algorithm_id":str(algorithm_id),
    "architecture_id":str(project_id) # TODO: remove this parameter. project_id is passed only to prevent restAPI errors, the architecture id is no longer required.
    }

    logging.debug("Request URL %s" % url)
    logging.debug("Request data %s" % str(data))
    try:
       r = requests.post(url,json=data)
       logging.info("Response status code %s" % r.status_code )

    except:
       r = ""
       logging.error ("Unable to connect to %s"%host)
       traceback.print_exc()

    logging.info('Callback_id: %s' % callback_id)

    try:
        json_resp = r.json()
        logging.debug(json_resp)
    except:
        logging.error ("No JSON object could be decoded.")


    try:
        job_id = json_resp['job_id']
    except KeyError:
        job_id = None
        logging.error ("There is no job_id key in the response.")
        logging.error (json_resp)



    return callback_id, job_id


def power_perf_eval_getresults (job_id, algorithm_id, **kwargs):
    host                       = "power_perf:5000"

    url='http://%s/api/power_performance_evaluations?project_id=%s&algorithm_configuration_id=fixme&architecture_id=fixme'%(host,str(job_id))


    try:
       r = requests.get(url)
       logging.info("Response status code %s" % r.status_code )

    except:
       r = ""
       logging.error ("Unable to connect to %s"%host)
       traceback.print_exc()
       return -1


    print (r.json())



    # retrive the algorithm
    logging.info('Retrieving algorithm from the DB...')
    try:
        algorithm = AlgorithmConfiguration.objects.get(id=ObjectId(algorithm_id))


    except AlgorithmConfiguration.DoesNotExist: # if the AlgorithmConfiguration doesn't exist return an error
        logging.error ("ERROR! Project doesn't exist")
        return -1
    except AlgorithmConfiguration.MultipleObjectsReturned:
        logging.error ("ERROR! Multiple AlgorithmConfiguration returned")
        return -1



    # Convert to json the output of the java application
    try:
        j = r.json()

        algorithm.performance    = j['performance'] #FIXME execution_time
        algorithm.energy         = j['energy']
        algorithm.processors     = j['processors']
        algorithm.memory         = j['memory']

        algorithm.save() # save in the DB a updated document containing the results of the evaluation
        logging.info ("Results saved in the DB")

    except Exception as e:
        logging.error ("ERROR: something went wrong!")
        logging.error (e)
        return -1



def power_perf_eval_getstatus (project_id=None, algorithm_id=None, **kwargs):
    host                       = "power_perf:5000"
    #project_id                 = kwargs.get("projectID", None)
    #algorithm_id               = kwargs.get("algorithm_id", None)

    '''
    #get results
    if r.json()['message']== 'finished':
        api='http://%s/api/eval_perf?algorithm_configuration_id=%s&project_id=%s&architecture_id=%s'%(host,algorithm_configuration_id, project_id, architecture_id)

        r = requests.get(api)

        print(r.json())
    elif r.json()['message']== 'failed':
        return 'failed'
    '''
    print ("perf eval status")



#  ██████╗  █████╗ ██████╗ ███████╗██╗███╗   ███╗    ██╗███╗   ██╗███████╗███████╗    ██╗    ██╗██████╗  █████╗ ██████╗
#  ██╔══██╗██╔══██╗██╔══██╗██╔════╝██║████╗ ████║    ██║████╗  ██║██╔════╝██╔════╝    ██║    ██║██╔══██╗██╔══██╗██╔══██╗
#  ██████╔╝███████║██████╔╝███████╗██║██╔████╔██║    ██║██╔██╗ ██║█████╗  █████╗      ██║ █╗ ██║██████╔╝███████║██████╔╝
#  ██╔═══╝ ██╔══██║██╔══██╗╚════██║██║██║╚██╔╝██║    ██║██║╚██╗██║██╔══╝  ██╔══╝      ██║███╗██║██╔══██╗██╔══██║██╔═══╝
#  ██║     ██║  ██║██║  ██║███████║██║██║ ╚═╝ ██║    ██║██║ ╚████║██║     ███████╗    ╚███╔███╔╝██║  ██║██║  ██║██║
#  ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝╚═╝     ╚═╝    ╚═╝╚═╝  ╚═══╝╚═╝     ╚══════╝     ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝
#


def parsim_start (project_id=None, algorithm_id=None, callback_id=None, **kwargs):



    host                       = "parsim_inference:5000"
   # project_id                 = kwargs.get("projectID", None)
   # algorithm_id    = kwargs.get("algorithm_configuration_id", None)

    logging.info('Refinement for parsimonious inference started. Callback_id: %s' % callback_id)
    send_back_status(project_id, "Running", "Refinement for parsimonious inference started", "rpi_en", "Running")

    #get a callback id
    if callback_id==None:
      callback_id = get_callback_id()
    
    
    # retrive the configurationFile
    try:
      config = ConfigurationFile.objects.get(project=str(project_id))

    except ConfigurationFile.DoesNotExist:
      logging.error ("No config file.")
      return callback_id, None


    # retrive the onnx model
    try:
      algorithm = AlgorithmConfiguration.objects.get(id=algorithm_id)

    except AlgorithmConfiguration.DoesNotExist:
      logging.error ("AlgorithmConfiguration doesn't exist.")
      return callback_id, None

    # retrive the dataset
    try:
      dataset = Dataset.objects(project=str(project_id)).get(pipeline=str(algorithm.pipeline)) #dataset = Dataset.objects.get(project=str(project_id))

    except Dataset.DoesNotExist:
      logging.error ("Dataset doesn't exist.")
      return callback_id, None



    project_path = "experiments/prj_%s/alg_%s/"%(str(project_id),str(algorithm_id))
    tensor_shape = "NHWC" # NCHW, NHWC
    regime       = "regimes/regime.json"
    
    dims = []
    for d in algorithm.input_size:
      if d>0:
        dims.append(d)
    
    
    if tensor_shape == "NHWC":
      input_shape = dims[-1:] + dims[0:-1]
    elif tensor_shape == "NCHW":
      input_shape = dims
      

    # start refinement parsimonious inference
    url='http://%s/api/rpi?callback_id=%s'%(host,callback_id)

    data={"algorithm_path" : str(algorithm.onnx_trained),
          "project_path"   : project_path,
          "dataset_path"   : str(dataset.path),
          "regime_path"    : regime,
          "tensor_shape"   : tensor_shape,
          "input_shape"   :  input_shape,
          "network"        : "null",
          "project_id"     : project_id}

    logging.debug("Request URL %s" % url)
    logging.debug("Request data %s" % str(data))

    try:
      r = requests.post(url,json=data)
    except:
      logging.error ("Connection error! Ensure RPI module is up.")
      send_back_status(project_id, "Errors", "RPI failed", "rpi_en", "Failed")
      return 1, None


    logging.info("Response status code %s" % r.status_code )

    try:
        json_resp = r.json()
        logging.debug(json_resp)
    except ValueError:
        logging.error ("No JSON object could be decoded.")


    try:
        job_id = json_resp['id'] #TODO the rpi tool returns id instead of job_id
    except KeyError:
        logging.error ("There is no job_id key in the response.")
        logging.error (json_resp)
        job_id = None


    return callback_id, job_id


def parsim_getresults (job_id = None, algorithm = None , **kwargs):

    host = "parsim_inference:5000"


    #get results
    url='http://%s/api/rpi/%s?'%(host,job_id)

    r = requests.get(url)


    logging.info("Response status code %s" % r.status_code )


    try:
        resp_json = r.json()

        algorithm.rpi_onnx_path         = resp_json["onnx_path"]
        algorithm.rpi_training_accuracy = resp_json["accuracy"]
        algorithm.rpi_training_loss     = resp_json["loss"]
        algorithm.rpi_quantization      = resp_json["quantization"];

        #insert the original accuracy and loss as first element

        algorithm.rpi_training_accuracy.insert(0,algorithm.validation_accuracy)
        algorithm.rpi_training_loss.insert(0,algorithm.validation_loss)
        algorithm.rpi_quantization.insert(0,(32,32))

        min_loss = algorithm.rpi_training_loss[0]
        threshold_loss = 1.05 * min_loss # 5% more than the minimimum


      #  m          = max(algorithm.rpi_training_loss)
        best_point = 0 #algorithm.rpi_training_loss.index(m)

        for i in range (0, len(algorithm.rpi_training_loss)):
          if algorithm.rpi_training_loss[i] < threshold_loss: # and algorithm.rpi_training_loss[i]<algorithm.rpi_training_loss[best_point]:
            best_point  = i

        point_dict = {'index'                 : best_point,
                      'rpi_training_accuracy' : algorithm.rpi_training_accuracy[best_point],
                      'rpi_training_loss'     : algorithm.rpi_training_loss[best_point],
                      'rpi_quantization'      : algorithm.rpi_quantization[best_point]}

        algorithm.rpi_best_quantization = point_dict

        try:
            algorithm.save()
            logging.info("RPI values saved in DB. Argorithm id: %s" % algorithm.get_id() )
        except:
            logging.error ("ERROR: Access to bd failed.")
            traceback.print_exc()

    except Exception as e:
        logging.error ("No valid JSON returned")
        return 'Error'
    return 'OK'


def parsim_getstatus (job_id):

    host                       = "parsim_inference:5000"


    #get results
    url='http://%s/api/rpi/%s?'%(host,job_id)

    r = requests.get(url)





# ████████╗██████╗  █████╗ ██╗███╗   ██╗██╗███╗   ██╗ ██████╗     ██╗    ██╗██████╗  █████╗ ██████╗
# ╚══██╔══╝██╔══██╗██╔══██╗██║████╗  ██║██║████╗  ██║██╔════╝     ██║    ██║██╔══██╗██╔══██╗██╔══██╗
#    ██║   ██████╔╝███████║██║██╔██╗ ██║██║██╔██╗ ██║██║  ███╗    ██║ █╗ ██║██████╔╝███████║██████╔╝
#    ██║   ██╔══██╗██╔══██║██║██║╚██╗██║██║██║╚██╗██║██║   ██║    ██║███╗██║██╔══██╗██╔══██║██╔═══╝
#    ██║   ██║  ██║██║  ██║██║██║ ╚████║██║██║ ╚████║╚██████╔╝    ╚███╔███╔╝██║  ██║██║  ██║██║
#    ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝╚═╝╚═╝  ╚═══╝ ╚═════╝      ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝


def training (project_id=None, algorithm_id=None, callback_id=None, **kwargs):



    host            = "training:5000"
   # project_id      = kwargs.get("projectID", None)
   # algorithm_id    = kwargs.get("algorithm_configuration_id", None)

            
    logging.info('Training started.')
    send_back_status(project_id, "Running", "Training started", "tra_en", "Running")

    #get a callback id
    if callback_id==None:
      callback_id = get_callback_id()

  # start a new training

    url='http://%s/api/training_call?project_id=%s&designpoint_id=%s&callback_id=%s'%(host, project_id, algorithm_id, callback_id )
    logging.debug("Request URL %s" % url)

    try:
      r = requests.post(url) #TODO: the training tool gets the parameters in the url. Fix it passing them in the body
    except:
      logging.error ("Connection error! Ensure training module is up.")
      send_back_status(project_id, "Errors", "training failed", "tra_en", "Failed")
      return 1, None

    logging.info("Response status code %s" % r.status_code )

   
    logging.info('Callback_id: %s' % callback_id)

    

    try:
        json_resp = r.json()
        logging.debug(json_resp)
    except ValueError:
        logging.error ("No JSON object could be decoded.")



    return callback_id, 1

def training_getStatus (callback_id):

    host                       = "training:5000"

    api='http://%s/api/training_respond?callback_id=%s'%(host, callback_id )
    r = requests.post(api)

    try:
        json_resp = r.json()
        logging.debug(json_resp)
    except ValueError:
        logging.error ("No JSON object could be decoded.")
    return json_resp['current_status']

def training_getstatus (**kwargs):

    host                       = "training:5000"
    project_id                 = kwargs.get("projectID", None)
    algorithm_id = kwargs.get("algorithm_configuration_id", None)

    api='http://%s/api/training_call?project_id=%s&designpoint_id=%s'%(host, project_id, algorithm_id )
    r = requests.post(api)



#  ███████╗███████╗ ██████╗██╗   ██╗██████╗ ██╗████████╗██╗   ██╗    ███████╗██╗   ██╗ █████╗ ██╗
#  ██╔════╝██╔════╝██╔════╝██║   ██║██╔══██╗██║╚══██╔══╝╚██╗ ██╔╝    ██╔════╝██║   ██║██╔══██╗██║
#  ███████╗█████╗  ██║     ██║   ██║██████╔╝██║   ██║    ╚████╔╝     █████╗  ██║   ██║███████║██║
#  ╚════██║██╔══╝  ██║     ██║   ██║██╔══██╗██║   ██║     ╚██╔╝      ██╔══╝  ╚██╗ ██╔╝██╔══██║██║
#  ███████║███████╗╚██████╗╚██████╔╝██║  ██║██║   ██║      ██║       ███████╗ ╚████╔╝ ██║  ██║███████╗
#  ╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝   ╚═╝      ╚═╝       ╚══════╝  ╚═══╝  ╚═╝  ╚═╝╚══════╝
#



def security_eval (project_id=None, dataset_id=None, algorithm_id=None, callback_id=None,**kwargs):

    #print (kwargs)
    # retrive the onnx model
    try:
      algorithm = AlgorithmConfiguration.objects.get(id=ObjectId(algorithm_id))

    except AlgorithmConfiguration.DoesNotExist:
      logging.error ("ERROR! AlgorithmConfiguration doesn't exist")
      return callback_id, None
    
    # retrive the project
    try:
      project = Project.objects.get(id=ObjectId(project_id))

    except Project.DoesNotExist:
      logging.error ("ERROR! Project doesn't exist")
      return callback_id, None

    # retrive the configurationFile
    try:
      config = ConfigurationFile.objects.get(project=str(project_id))

    except ConfigurationFile.DoesNotExist:
      logging.error ("No config file.")
      return callback_id, None


    host                       = "security:5000"

    logging.info('Security evaluation started. Callback_id: %s' % callback_id)
    send_back_status(project_id, "Running", "Security evaluation started", "sec_en", "Running")

    #get a callback id
    if callback_id==None:
      callback_id = get_callback_id()

    # retrive the dataset
    try:
      dataset = Dataset.objects(project=str(project_id)).get(pipeline=str(algorithm.pipeline)) #Dataset.objects.get(project=str(project_id))

    except Dataset.DoesNotExist:
      logging.error ("ERROR! Dataset doesn't exist")
      return callback_id, None


    metrics = {'classification':'classification-accuracy',
              'detection':'map',
              'segmentation':'iou'}
    req_body = {
        "dataset": dataset.path,
        "trained-model": algorithm.onnx_trained,
        "performance-metric": metrics[config.task_type],
        "perturbation-type": "max-norm",
        "evaluation-mode": "fast",
        "task": config.task_type,
        "config-path"  : os.path.join(os.path.split(os.path.split(algorithm.onnx_trained)[0])[0], "AlgorithmConfiguration.json"),
        "pipeline-path": os.path.join(os.path.split(os.path.split(algorithm.onnx_trained)[0])[0], "preprocessing_pipeline.json"),
        "perturbation-values": [
          0, 0.0005, 0.001, 0.002, 0.01, 0.05, 0.1, 0.25, 0.5
        ]
      }


#    req_body = {
#    "dataset"             : str(dataset.id),
#    "project_id"          : project_id,
#    "trained-model"       : algorithm_id,
#    "performance-metric"  : "classification-accuracy",
#    "perturbation-type"   : "max-norm",
#    "perturbation-values" : [0, 0.001, 0.002, 0.003, 0.005, 0.0075, 0.01, 0.02]
#    }

    # start Security evaluation
    url = 'http://%s/api/security_evaluations?callback_id=%s'%(host,callback_id)


    logging.debug("Request URL %s" % url)
    logging.debug("Request data %s" % str(req_body))


    r = requests.post(url, json=req_body)

    logging.info("Response status code %s" % r.status_code )

    #print (r.text)

    job_id = (r.text)[1:-2] #TODO: security tool doesn't return a json. fix it.

    '''
    try:
        json_resp = r.json()
        logging.debug(json_resp)
    except ValueError:
        logging.error ("No JSON object could be decoded.")


    try:
        job_id = json_resp['job_id']
    except KeyError:
        logging.error ("There is no job_id key in the response.")
        logging.error (json_resp)
    '''


    return callback_id, job_id


def security_eval_getresults(job_id = None, algorithm = None,**kwargs):

    logging.info("Retrieving the security results..." )

    host                       = "security:5000"

    url = 'http://%s/api/security_evaluations/%s/output'%(host, job_id)

    logging.debug("Request URL %s" % url)

    r=requests.get(url)

    logging.info("Response status code %s" % r.status_code )


    try:
        resp_json = r.json()
        algorithm.sec_level = resp_json["sec-level"];
        algorithm.sec_value = resp_json["sec-value"];
        algorithm.sec_curve = resp_json["sec-curve"];

        try:
            algorithm.save()
            logging.info("Security values saved in DB. Argorithm id: %s" % algorithm.get_id() )
        except:
            logging.error ("ERROR: Access to bd failed.")
            traceback.print_exc()
            return 'Error'

    except Exception as e:
        logging.error ("No valid JSON returned")
        traceback.print_exc()
        return 'Error'

    return 'OK'

def security_eval_getimage(job_id = None, algorithm = None,**kwargs):

    logging.info("Retrieving the security results image..." )

    host                       = "security:5000"

    url = 'http://%s/api/security_evaluations/%s/output/stored'%(host, job_id)

    logging.debug("Request URL %s" % url)

    r=requests.get(url)

    logging.info("Response status code %s" % r.status_code )



#try:

    # connect to the DB
    #mongo = MongoConnector()
    #print (resp_json)
    algorithm.security_img_path = r.text[1:-2]; #TODO security tool doesn't return a json
    algorithm.save()
    logging.info("Security image path saved in DB. Argorithm id: %s" % algorithm.get_id() )
#    except:
#        logging.error ("ERROR: Access to bd failed.")

def security_eval_getstatus(job_id = None,**kwargs):


    host                       = "security:5000"

    url = 'http://%s/api/security_evaluations/%s'%(host, job_id)

    r=requests.get(url)


    print (r.status_code)

    try:
        print (r.json())
    except:
        print ("No JSON returned")


    print ('Add to onnx class')
