
#  ███╗   ███╗ █████╗ ██╗███╗   ██╗
#  ████╗ ████║██╔══██╗██║████╗  ██║
#  ██╔████╔██║███████║██║██╔██╗ ██║
#  ██║╚██╔╝██║██╔══██║██║██║╚██╗██║
#  ██║ ╚═╝ ██║██║  ██║██║██║ ╚████║
#  ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝
             
from satellite_tools import *
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Satellite tools API')
    parser.add_argument('tool', type=str, choices=['rpi', 'power_perf', 'security', 'training', 'pareto'], help='Select the tool to be executed.')
    parser.add_argument('-i', '--project',       default=None, type=str, help='The id of the current project')
    parser.add_argument('-a', '--algorithm',     default=None, type=str, help='The id of the algorithm configuration to be evaluated')
    parser.add_argument('-m', '--pmetric',     default=None, type=str, help='The metric you want to log in tensorboard')
    args = parser.parse_args()
    
    #redisc=RedisConnector()
    #redisc.conn.set("a", '1')
    #exit(0)
    if args.tool == "rpi":
        parsim_cb, job_id = parsim_start (project_id=args.project, algorithm_id=args.algorithm)
        barrier_cb(parsim_cb)
        
    elif args.tool == "power_perf":
        print ("Satellite tool: power_perf")
        power_perf_eval_cb, job_id=power_perf_eval_start (project_id=args.project, algorithm_id=args.algorithm)
        print ("{'callback_id':'%s', 'job_id':'%s'}"%(power_perf_eval_cb, job_id))
        #barrier_cb(power_perf_eval_cb)
        print ("Power_perf: DONE!")     
    
    elif args.tool == "security":
        security_eval_cb, job_id = security_eval (project_id=args.project, algorithm_id=args.algorithm)
        barrier_cb(security_eval_cb)
        
        security_eval_getresults(job_id = job_id, algorithm = algorithm_configuration)

        security_eval_getimage  (job_id = job_id, algorithm  = algorithm_configuration)
    
    elif args.tool == "training":
        print ("Satellite tool: training")
        training_cb = training (project_id=args.project, algorithm_id=args.algorithm)
        #barrier_cb(training_cb) # wait for the end of the training.
        
        
    elif args.tool == "pareto":
        #training_cb = training (project_id=args.project, algorithm_id=args.algorithm)
        #barrier_cb(training_cb) # wait for the end of the training.
        print ("b")

