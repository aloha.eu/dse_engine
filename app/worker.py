"""
For more workers use the following command:

docker-compose up --scale worker=2
"""

import os

from redis import Redis
from rq import Worker, Queue, Connection
'''
from flask import Flask, url_for, redirect
from settings import settings
from models.models import *
from db.mongo import MongoConnector
'''

# use rq-dashboard for visualization
listen = [os.environ.get('REDIS_QUEUE', 'default')]

REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')
REDIS_PORT = os.environ.get('REDIS_PORT', '6379')


print ("Redis: " + REDIS_HOST)
print (REDIS_PORT)

conn = Redis(host=REDIS_HOST, port=REDIS_PORT)


if __name__ == '__main__':

    with Connection(conn):
        q = Queue()

        worker = Worker(list(map(Queue, listen)))
        worker.work()
